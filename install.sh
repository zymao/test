#!/usr/bin/env bash
set -e
set -x

LIB_DIR=/var/lib/clamav
SHARE_PATH=/usr/local/share/clamav
INSTALL_PACKAGE=/root/clamav/clamav-0.105.1.linux.x86_64.rpm

function installClamAV() {
  uname -m | grep x86_64
  isArm=$?
  if [ ${isArm} = 1 ]; then
    return
  fi

  echo "start installing ..."
  
  apt install rpm
  rpm -ivh "${INSTALL_PACKAGE}"

  echo "install finished, begin configuring ..."

  cp /usr/local/etc/freshclam.conf.sample /usr/local/etc/freshclam.conf
  cp /usr/local/etc/clamd.conf.sample /usr/local/etc/clamd.conf
  chmod 644 /usr/local/etc/clamd.conf /usr/local/etc/freshclam.conf

  echo "configuring clamd.conf ..."
  sed -i s/Example/#Example/g /usr/local/etc/clamd.conf
  sed -i s@'#LogFile /tmp/clamd.log'@'LogFile /var/log/clamd.log'@g /usr/local/etc/clamd.conf
  sed -i s@'#LogClean yes'@'LogClean yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#LogTime yes'@'LogTime yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#LogVerbose yes'@'LogVerbose yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#LogRotate yes'@'LogRotate yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ExtendedDetectionInfo yes'@'ExtendedDetectionInfo yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#DatabaseDirectory /var/lib/clamav'@'DatabaseDirectory /var/lib/clamav'@g /usr/local/etc/clamd.conf
  sed -i s@'#TCPSocket 3310'@'TCPSocket 3310'@g /usr/local/etc/clamd.conf
  sed -i s@'#TCPAddr localhost'@'TCPAddr 127.0.0.1'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanOLE2 yes'@'ScanOLE2 yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanPDF yes'@'ScanPDF yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanSWF yes'@'ScanSWF yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanXMLDOCS yes'@'ScanXMLDOCS yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanHWP3 yes'@'ScanHWP3 yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanMail yes'@'ScanMail yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#ScanPartialMessages yes'@'ScanPartialMessages yes'@g /usr/local/etc/clamd.conf
  sed -i s@'#MaxScanSize 150M'@'MaxScanSize 1024M'@g /usr/local/etc/clamd.conf
  sed -i s@'#MaxFileSize 30M'@'MaxFileSize 1024M'@g /usr/local/etc/clamd.conf
  echo "clamd.conf done"

  echo "configuring freshclam.conf ..."
  sed -i s/Example/#Example/g /usr/local/etc/freshclam.conf
  sed -i s@'#UpdateLogFile /var/log/freshclam.log'@'UpdateLogFile /var/log/freshclam.log'@g /usr/local/etc/freshclam.conf
  sed -i s@'#DatabaseDirectory /var/lib/clamav'@'DatabaseDirectory /var/lib/clamav'@g /usr/local/etc/freshclam.conf
  sed -i s@'#LogTime yes'@'LogTime yes'@g /usr/local/etc/freshclam.conf
  sed -i s@'#LogVerbose yes'@'LogVerbose yes'@g /usr/local/etc/freshclam.conf
  sed -i s@'#LogRotate yes'@'LogRotate yes'@g /usr/local/etc/freshclam.conf
  sed -i s@'#DatabaseOwner clamav'@'DatabaseOwner root'@g /usr/local/etc/freshclam.conf
  sed -i s@'#OnUpdateExecute command'@'OnUpdateExecute chmod 644 /var/lib/clamav/*'@g /usr/local/etc/freshclam.conf
  sed -i s@'OnOutdatedExecute command'@'OnOutdatedExecute freshclam'@g /usr/local/etc/freshclam.conf
  echo "freshclam.config done"

  if [ ! -d ${LIB_DIR} ]; then
    mkdir -p ${LIB_DIR}
  fi
  if [! -d ${SHARE_PATH} ]; then
    mkdir -p ${SHARE_PATH}
  fi
  chmod 700 ${LIB_DIR} ${SHARE_PATH}

  touch /var/log/clamd.log
  touch /var/log/freshclam.log
  chmod 644 /var/log/clamd.log /var/log/freshclam.log

  freshclam && chmod 644 /var/lib/clamav/*

  clamdscan -V | grep 'ClamAV 0.105.1'
  grepV01051=$?
  if [ ${grepV01051} = 0 ]; then
    echo "clamav installed success"
  fi 
}

installClamAV
